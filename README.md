## Conceito de POD ##

 - O POD é o menor objeto que se pode criar no kubernetes
 - Se precisarmos escalar a aplicação, adicionamos uma nova instância de um POD com a mesma aplicação
 - Geralmente a relação é de um POD para um Container
 - Com multicontainers, um mesmo POD pode ter multiplos containers que geralmente não são do mesmo tipo 
 - É possível criar um pod com o seguinte comando: kubectl create -f < arquivo yaml >
    - kubectl create -f pod-definition.yaml




![Alt text](imagem/k8s-pod.png)
